# Spring Boot Angular Blog

This is a demo Spring Boot & Angular blog app with the following functionality:

- User can log in
- User can log out
- User can create a post with a rich text editor
- The post can contain images and HTML
- Everybody can view a post

## Technologies & tools

- Java 11
- Spring Boot 2.3.1
- Hibernate
- Angular 7
- Maven
- MariaDB
- Lombok

## Links

- [Video tutorials](https://www.youtube.com/playlist?list=PLSVW22jAG8pCwwM3tjSMfwBKYIS6_fP-F)
- [Text tutorials](https://programmingtechie.com/2019/09/01/lets-build-a-simple-blog-using-spring-boot-and-angular-part-1/)
- [Spring Security: Authentication and Authorization In-Depth](https://www.marcobehler.com/guides/spring-security)

## Database schema

```
+------------------------+
| Tables_in_ngspringblog |
+------------------------+
| post                   |
| user                   |
+------------------------+
```

`post` table:
```
+------------+--------------+------+-----+---------+----------------+
| Field      | Type         | Null | Key | Default | Extra          |
+------------+--------------+------+-----+---------+----------------+
| id         | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| content    | longtext     | YES  |     | NULL    |                |
| created_on | datetime(6)  | YES  |     | NULL    |                |
| title      | varchar(255) | YES  |     | NULL    |                |
| updated_on | datetime(6)  | YES  |     | NULL    |                |
| username   | varchar(255) | YES  |     | NULL    |                |
+------------+--------------+------+-----+---------+----------------+
```

`user` table:

```
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
| username | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
```

## API

`{base_url} = localhost:8080`

#### Signup

This request creates a new entry in `user` table. The password is encrypted.
For now, there is no check up for existing users (username and email are not unique), so this method can produce duplicates.

```yaml
url: "{base_url}/api/auth/signup"
method: POST
  
headers:
    Content-Type: application/json  
  
json:
    {
        "email": "test@mail",
        "username": "Testuser",
        "password": "testpass"
    }
```
Success response:

```yaml
status_code: 200
response_body:
    "user added"
```


#### Authentication

Authenticates a user and if successful, returns his token

```yaml
url: "{base_url}/api/auth/login"
method: POST
  
headers:
    Content-Type: application/json  
  
json:
{
	"username": "Testuser",
	"password": "testpass"
}
```
Success response:

```yaml
status_code: 200
response_body: a JWT
```

Response if the user was not successfully authenticated:

```yaml
status_code: 403
```

#### Create a post

Validates logged in user's JWT and, if successful, creates a post with a title and content. The author will be current logged in user.

```yaml
url: "{base_url}/api/posts/"
method: POST
  
headers:
    Content-Type: application/json
    Authentication: Bearer {jwt}
  
json:
{
	"title": "Post's title",
	"content": "some text..."
}
```

Success response:

```yaml
status_code: 200
```



#### Get all posts

Get all posts from the table `post`. No pagination.

```yaml
url: "{base_url}/api/posts/all"
method: GET
  
headers:
    Content-Type: application/json
    Authentication: Bearer {jwt}
```

Success response:

```yaml
status_code: 200
json:
[
    {
        "id": 1,
        "content": "This is come content",
        "title": "This is a title",
        "username": "Enio"
    },
    ...
]
```



#### Get a post by id

Get a post by id from the table `post`. 

```yaml
url: "{base_url}/api/posts/get/{id}"
method: GET
  
headers:
    Content-Type: application/json
    Authentication: Bearer {jwt}
```

Success response:

```yaml
status_code: 200
json:
{
    "id": 2,
    "content": "Second content",
    "title": "Second post",
    "username": "Enio"
}
```

