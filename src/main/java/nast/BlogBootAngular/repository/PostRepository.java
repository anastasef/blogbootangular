package nast.BlogBootAngular.repository;

import nast.BlogBootAngular.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {

}
