package nast.BlogBootAngular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogBootAngularApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogBootAngularApplication.class, args);
	}

}
