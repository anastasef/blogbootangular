package nast.BlogBootAngular.service;

import nast.BlogBootAngular.dto.PostDto;
import nast.BlogBootAngular.exception.PostNotFoundException;
import nast.BlogBootAngular.model.Post;
import nast.BlogBootAngular.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostService {

    @Autowired
    private AuthService authService;

    @Autowired
    private PostRepository postRepository;

    public void createPost(PostDto postDto) {
        Post post = mapFromDtoToPost(postDto);
        postRepository.save(post);
    }

    public List<PostDto> showAllPosts() {
        List<Post> posts = postRepository.findAll();
        return posts.stream()
                .map(this::mapFromPostToDto)
                .collect(Collectors.toList());
    }

    private PostDto mapFromPostToDto(Post post) {
        PostDto postDto = new PostDto();
        postDto.setId(post.getId());
        postDto.setTitle(post.getTitle());
        postDto.setContent(post.getContent());
        postDto.setUsername(post.getUsername());
        return postDto;
    }

    private Post mapFromDtoToPost(PostDto postDto) {
        Post post = new Post();
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        // Get current user
        User loggedInUser = authService.getCurrentUser()
                .orElseThrow(() -> new IllegalArgumentException("No user logged in!"));
        post.setUsername(loggedInUser.getUsername()); // Post's author = current user
        post.setCreatedOn(Instant.now());
        return post;
    }

    public PostDto readSinglePost(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new PostNotFoundException("Post with id = " + id + " not found"));
        return mapFromPostToDto(post);
    }
}
