package nast.BlogBootAngular.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Data transferred between the backend and the frontend in the POST /api/auth/login request.
 */
@Getter
@Setter
public class LoginRequest {
	
    private String username;
    private String password;

}
