package nast.BlogBootAngular.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Data transferred between the backend and the frontend in the POST /api/auth/signup request.
 */
@Setter
@Getter
public class RegisterRequest {

    private String username;
    private String password;
    private String email;

}
