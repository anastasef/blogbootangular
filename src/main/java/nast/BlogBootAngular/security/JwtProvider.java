package nast.BlogBootAngular.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;

import nast.BlogBootAngular.exception.SpringBlogException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

@Service
public class JwtProvider {

	private KeyStore keyStore;
	private final String KEYSTORE_PATH = "/springblog.jks";
	private final String KEYSTORE_PASS = "cyberseal";
	private final String KEYS_ALIAS = "springblog";

    @PostConstruct
    public void init() {
    	
    		try {
				keyStore = KeyStore.getInstance("JKS");
			} catch (KeyStoreException e) {
				e.printStackTrace();
			}
    		
    		InputStream keystoreAsStream = getClass().getResourceAsStream(KEYSTORE_PATH);
    		
    		// KeyStore.load() resets the key store to the empty state when passed a null input stream,
    		// so we need to be sure that the file was found.
    		if ( keystoreAsStream == null ) throw new SpringBlogException("keystore file was not found" );
    		
			try {
				keyStore.load(keystoreAsStream, KEYSTORE_PASS.toCharArray());
			} catch (NoSuchAlgorithmException | CertificateException | IOException e) {
				throw new SpringBlogException("Exception occurred while loading keystore: " + e.getMessage());
			}
    }

    public String generateToken(Authentication authentication) {
        User principal = (User) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(principal.getUsername())
                .signWith(getPrivateKey())
                .compact();
    }

	/**
     * @param jwt client's JWT
     * @return if the JWT's signature is valid or throw an exception
     */
    public boolean validateToken(String jwt) {
        try {
            Jwts.parserBuilder().setSigningKey(getPublicKey()).build().parseClaimsJws(jwt);
        } catch (SignatureException e) {
            return false;
        }
        return true;
    }

	public String getUsernameFromJWT(String jwt) {
        Claims claims = Jwts.parserBuilder().setSigningKey(getPublicKey()).build()
                .parseClaimsJws(jwt)
                .getBody();
        return claims.getSubject();
    }
    
    private PrivateKey getPrivateKey() {
    	try {    		
    		if ( !keyStore.containsAlias(KEYS_ALIAS) ) 
    			throw new SpringBlogException("KeyStore does not contain the alias \"" + KEYS_ALIAS + "\"");
    		
			PrivateKey privateKey = (PrivateKey) keyStore.getKey(KEYS_ALIAS, KEYSTORE_PASS.toCharArray());
			return privateKey;
		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
			throw new SpringBlogException("Exception occurred while retrieving private key from keystore");
		}
	}
    
    private PublicKey getPublicKey() {
    	try {
			return keyStore.getCertificate(KEYS_ALIAS).getPublicKey();
		} catch (KeyStoreException e) {
			throw new SpringBlogException("Exception occurred while retrieving public key from keystore");
		}
	}
    
}
